﻿using DataBassClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBassClient.Helpers
{
    public class PrintHelper
    {
        /// <summary>
        /// Will display a dictionary list of customers to the console with key value pairs
        /// </summary>
        /// <param name="listToPrint"></param>
        public static void PrintListKv(Dictionary<string, int> listToPrint)
        {
            foreach (var keyValuePair in listToPrint)
            {
                Console.Write(keyValuePair.Key + " ");
                Console.WriteLine(keyValuePair.Value);
            }
        }
        /// <summary>
        /// Will display total songs with same genre and the genre to the console
        /// </summary>
        /// <param name="listToPrint"></param>
        public static void PrintMaxDictValue(Dictionary<string, int> listToPrint)
        {
            int max = listToPrint.Values.Max();
            List<string> genres = new List<string>();

            Console.WriteLine($"Max genre count: {max}");
            foreach (var keyValuePair in listToPrint)
            {
                if (keyValuePair.Value == max)
                    Console.WriteLine(keyValuePair.Key);
            }
        }
        /// <summary>
        /// Will display a list of customer by their total invoices ordered by desc to the console
        /// </summary>
        /// <param name="totalCustomerInvoices"></param>
        public static void PrintCustomersByTotalInvoices(Dictionary<int, decimal> totalCustomerInvoices)
        {
            foreach (var keyValuePair in totalCustomerInvoices)
            {
                Console.Write("Customer Id: " + keyValuePair.Key + " ");
                Console.WriteLine("Total money spent: " + keyValuePair.Value);
            }
        }
    }
}
