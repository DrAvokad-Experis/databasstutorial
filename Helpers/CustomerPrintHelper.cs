﻿using DataBassClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBassClient.Helpers
{
    internal class CustomerPrintHelper : PrintHelper
    {
        /// <summary>
        /// Will display a single customer in console
        /// </summary>
        /// <param name="customer"></param>
        public static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"Customer {customer.ID}: {customer.FirstName}, {customer.LastName}, {customer.Country}, " +
                    $"{customer.PostalCode}, {customer.PhoneNumber}, {customer.Email}");
        }
        /// <summary>
        /// Will display a list of customers in console
        /// </summary>
        /// <param name="customers"></param>
        public static void PrintCustomers(List<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }
    }
}
