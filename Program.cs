﻿using DataBassClient.Helpers;
using DataBassClient.Models;
using DataBassClient.Repositories;

namespace DataBassClient
{
    public class Program
    {
        public static void Main(string[] args)
        {
            ICustomerRepository customerRepo = new SqlClientCustomerHelper();
            List<Customer> customers = customerRepo.ReadAll();
            foreach (Customer customer in customers)
            {
                CustomerPrintHelper.PrintCustomer(customer);
            }

            Customer customer1 = customerRepo.FindById(10);
            CustomerPrintHelper.PrintCustomer(customer1);

            Customer customer2 = customerRepo.FindByName("Leon");
            CustomerPrintHelper.PrintCustomer(customer2);

            List<Customer> customerList = customerRepo.ReadPage(10, 20);
            CustomerPrintHelper.PrintCustomers(customerList);

            Customer customer3 = new Customer
            {
                FirstName = "Alfons",
                LastName = "Åberg",
                Country = "Sweden",
                PostalCode = "42666",
                PhoneNumber = "133766642",
                Email = "Alfons@email.se"
            };

            CustomerPrintHelper.PrintCustomer(customerRepo.Create(customer3));


            Customer customer4 = new Customer
            {
                FirstName = "Alfonsio",
                LastName = "Aberg",
                Country = "America",
                PostalCode = "42626",
                PhoneNumber = "0702113442",
                Email = "Alfons@email.tech"
            };

            customerRepo.Update(customer4, 30);

            CustomerPrintHelper.PrintListKv(customerRepo.GetCustomersByCountry());

            CustomerPrintHelper.PrintCustomersByTotalInvoices(customerRepo.GetCustomersByTotalInvoice());

            PrintHelper.PrintMaxDictValue(customerRepo.GetCustomerMostPlayedGenre(2));
        }
    }
}