# Databass SQL

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Console App in C# for manipulating database

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background

This project was created for an assignment to learn more about SQL.


## Install

Install Visual Studio. Clone this repository and open with Visual Studio.
Install SQL Service Management Studio 18 and run the Chinook initialization script to create database.

## Usage

Go to test project and run SQL functions. 

## Maintainers

[Alexander Grönberg (@DrAvokad)](https://gitlab.com/DrAvokad)
[Rasmus Möller (@Achieuw)](https://gitlab.com/Achieuw)
[Aldin Drobic (@Adrobic)](https://gitlab.com/Adrobic)

## Acknowledgements

Thanks to Noroff for providing videos and lectures about C# and SQL

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Alexander Grönberg, Rasmus Möller, Aldin Drobic
