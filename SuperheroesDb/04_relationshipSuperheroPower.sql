CREATE TABLE SuperheroPowers(
	SuperheroID int FOREIGN KEY REFERENCES Superhero(ID),
	SuperPowerID int FOREIGN KEY REFERENCES SuperPower(ID)
)