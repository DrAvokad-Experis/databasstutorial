CREATE TABLE Superhero(
	ID int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	HeroName nvarchar(50) NOT NULL,
	Alias nvarchar(50) NULL,
	Origin nvarchar(50) NOT NULL
);

CREATE TABLE Assistant(
	ID int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	AssistantName nvarchar(50) NOT NULL
);

CREATE TABLE SuperPower(
	ID int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	PowerName nvarchar(50) NOT NULL,
	Description nvarchar(250) NULL
)