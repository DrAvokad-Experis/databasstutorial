INSERT INTO SuperPower VALUES ('Styrofoam Entombment', 'Entombs someone in styrofoam. Suffocation may occur.')
INSERT INTO SuperPower VALUES ('Cheese Deluge', 'Unleash a deluge of cheese large enough to devour a nation.')
INSERT INTO SuperPower VALUES ('Lightly Salt', 'Shake out a small amount of salt onto a surface.')
INSERT INTO SuperPower VALUES ('Oversalt', 'Add way too much salt to something rendering it useless in any context')

INSERT INTO SuperheroPowers (SuperheroID, SuperPowerID) VALUES (1, 1)
INSERT INTO SuperheroPowers (SuperheroID, SuperPowerID) VALUES (2, 3)
INSERT INTO SuperheroPowers (SuperheroID, SuperPowerID) VALUES (2, 4)
INSERT INTO SuperheroPowers (SuperheroID, SuperPowerID) VALUES (3, 2)
INSERT INTO SuperheroPowers (SuperheroID, SuperPowerID) VALUES (3, 3)