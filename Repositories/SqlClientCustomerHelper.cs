﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataBassClient.Models;
using Microsoft.Data.SqlClient;

namespace DataBassClient.Repositories
{
    public class SqlClientCustomerHelper : ICustomerRepository
    {
        /// <summary>
        /// Will create a customer
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public Customer Create(Customer customer)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    string sql = "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email)" +
                                $"VALUES('{customer.FirstName}', '{customer.LastName}', '{customer.Country}', '{customer.PostalCode}', '{customer.PhoneNumber}', '{customer.Email}')";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return customer;
        }
        /// <summary>
        /// Will delete a customer
        /// </summary>
        /// <param name="id"></param>
        /// <exception cref="NotImplementedException"></exception>
        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Will find a customer by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Customer FindById(int id)
        {
            Customer customer = new Customer();

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    string sql = $"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId = {id}";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                                customer = new Customer
                                {
                                    ID = reader.GetInt32(0),
                                    FirstName = reader.GetString(1),
                                    LastName = reader.GetString(2),
                                    Country = reader.GetString(3),
                                    PostalCode = reader.GetString(4),
                                    PhoneNumber = reader.GetString(5),
                                    Email = reader.GetString(6)
                                };
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return customer;
        }
        /// <summary>
        /// Will find a customer by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Customer FindByName(string name)
        {
            Customer customer = new Customer();

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    string sql = $"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE FirstName LIKE '%{name}%' OR LastName LIKE '%{name}%'";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                                customer = new Customer
                                {
                                    ID = reader.GetInt32(0),
                                    FirstName = reader.GetString(1),
                                    LastName = reader.GetString(2),
                                    Country = reader.GetString(3),
                                    PostalCode = reader.GetString(4),
                                    PhoneNumber = reader.GetString(5),
                                    Email = reader.GetString(6)
                                };
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return customer;
        }
        /// <summary>
        /// Will read all customer 
        /// </summary>
        /// <returns></returns>
        public List<Customer> ReadAll()
        {
            List<Customer> customers = new List<Customer>();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Handle result
                                Customer customer = new Customer();
                                customer.ID = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                if (!reader.IsDBNull(3)) customer.Country = reader.GetString(3);
                                if (!reader.IsDBNull(4)) customer.PostalCode = reader.GetString(4);
                                if (!reader.IsDBNull(5)) customer.PhoneNumber = reader.GetString(5);
                                customer.Email = reader.GetString(6);

                                customers.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


            return customers;
        }
        /// <summary>
        /// Reads a list of customers based on a limit and offset 
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public List<Customer> ReadPage(int limit, int offset)
        {
            List<Customer> customers = new List<Customer>();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    string sql = $"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer ORDER BY CustomerID OFFSET {offset} ROWS FETCH NEXT {limit} ROWS ONLY";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                                customers.Add(new Customer()
                                {
                                    ID = reader.GetInt32(0),
                                    FirstName = reader.GetString(1),
                                    LastName = reader.GetString(2),
                                    Country = reader.GetString(3),
                                    PostalCode = reader.GetString(4),
                                    PhoneNumber = reader.GetString(5),
                                    Email = reader.GetString(6)
                                });
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


            return customers;
        }
        /// <summary>
        /// Will update a customer
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="id"></param>
        public void Update(Customer customer, int id)
        {
            try
            {
                string sql = "UPDATE Customer " +
                    "SET FirstName = @FirstName, LastName = @LastName, Country = @Country, PostalCode = @PostalCode, Phone = @PhoneNumber, Email = @Email " +
                    "WHERE CustomerId = @CustomerId";
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        command.Parameters.AddWithValue("@LastName", customer.LastName);
                        command.Parameters.AddWithValue("@Country", customer.Country);
                        command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        command.Parameters.AddWithValue("@PhoneNumber", customer.PhoneNumber);
                        command.Parameters.AddWithValue("@Email", customer.Email);
                        command.Parameters.AddWithValue("@CustomerId", id);

                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        /// <summary>
        /// Will return a number of total customers/country and order by desc
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, int> GetCustomersByCountry()
        {
            Dictionary<string, int> userCountries = new Dictionary<string, int>();

            try
            {
                string sql = "select COUNT(CustomerId), Country from Customer GROUP BY Country ORDER BY COUNT(CustomerId) DESC;";
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                userCountries.Add(reader.GetString(1), reader.GetInt32(0));
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return userCountries;

        }
        /// <summary>
        /// Will return a list of customer by their total invoices ordered by desc
        /// </summary>
        /// <returns></returns>
        public Dictionary<int, decimal> GetCustomersByTotalInvoice()
        {
            Dictionary<int, decimal> totalCustomerInvoices = new Dictionary<int, decimal>();

            try
            {
                string sql = "select SUM(Total) AS TotalMoneySpent, CustomerId from Invoice GROUP BY CustomerId ORDER BY SUM(Total) DESC";
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                totalCustomerInvoices.Add(reader.GetInt32(1), reader.GetDecimal(0));
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return totalCustomerInvoices;

        }
        /// <summary>
        /// Will get a specific customers most played genre
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Dictionary<string, int> GetCustomerMostPlayedGenre(int id)
        {
            Dictionary<string, int> totalGenres = new Dictionary<string, int>();

            try
            {
                string sql = "SELECT COUNT(g.GenreId) AS GenreCount, g.Name " +
                             "FROM Invoice AS iv INNER JOIN InvoiceLine AS ivl " +
                             "ON iv.InvoiceId = ivl.InvoiceId " +
                             "INNER JOIN Track AS t ON ivl.TrackId = t.TrackId " +
                             "INNER JOIN Genre AS g " +
                             "ON t.GenreId = g.GenreId " +
                             "WHERE CustomerId = @CustomerId " +
                             "GROUP BY g.GenreId, g.Name ORDER BY COUNT(g.GenreId) DESC";
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@CustomerId", id);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                totalGenres.Add(reader.GetString(1), reader.GetInt32(0));
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return totalGenres;

        }
    }
}
