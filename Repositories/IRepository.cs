﻿using DataBassClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBassClient.Repositories
{
    public interface IRepository<T>
    {
        /// <summary>
        /// Used for creating a customer
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Customer Create(T entity);
        /// <summary>
        /// Reads all the customers
        /// </summary>
        /// <returns></returns>
        List<T> ReadAll();
        /// <summary>
        /// Reads a list of customers based on a limit and offset
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<T> ReadPage(int limit, int offset);
        /// <summary>
        /// Finds customer by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T FindById(int id);
        /// <summary>
        /// Will update a customer
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="id"></param>
        void Update(T entity, int id);
        /// <summary>
        /// Will delete a customer
        /// </summary>
        /// <param name="id"></param>
        void Delete(int id);
    }
}
