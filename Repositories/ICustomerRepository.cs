﻿using DataBassClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBassClient.Repositories
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        /// <summary>
        /// Will find a customer by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        Customer FindByName(string name);
        /// <summary>
        /// Will return a number of total customers/country and order by desc
        /// </summary>
        /// <returns></returns>
        Dictionary<string, int> GetCustomersByCountry();
        /// <summary>
        /// Will return a list of customer by their total invoices ordered by desc
        /// </summary>
        /// <returns></returns>
        Dictionary<int, decimal> GetCustomersByTotalInvoice();
        /// <summary>
        /// Will get a specific customers most played genre
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Dictionary<string, int> GetCustomerMostPlayedGenre(int id);
    }
}
